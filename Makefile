.PHONY : help clean veryclean prune docker push rmi build update work docker-debian11 debian11 docker-mint20 mint20 docker-mint21 mint21 docker-ubuntu20 ubuntu20 docker-ubuntu21 ubuntu21 docker-ubuntu22 ubuntu22 docker-fedora37 fedora37 docker-fedora38 fedora38 docker-macos-x86_64 macos-x86_64 docker-macos-aarch64 macos-aarch64 docker-tumbleweed tumbleweed tarball docker-dind windows

version:=$(shell cat version)
release:=$(shell cat release)
source_release:=$(shell cat source_release)
full_version:=$(version)-$(source_release)$(shell [ $(release) -gt 1 ] && echo "-$(release)")

help :
	@echo "Use: make [help]"
	@echo "          [docker] [push] [rmi]    - handle docker images"
	@echo "          [build]                  - building all artifacts"
	@echo "          [clean] [veryclean]      - cleaning up"
	@echo "          [update] [prune]         - misc."
	@echo ""
	@echo "docker targets:" 
	@echo "  [docker-debian11], [docker-debian12],[docker-mint20], [docker-mint21]"
	@echo "  [docker-ubuntu20], [docker-ubuntu21], [docker-ubuntu22]"
	@echo "  [docker-fedora37], [docker-fedora38]"
	@echo "  [docker-macos-x86_64], [docker-macos-aarch64]"
	@echo "  [docker-tumbleweed]"
	@echo "  [docker-dind]"
	@echo "  [docker-windows]"
	@echo ""
	@echo "build targets:" 
	@echo "  [debian11], [debian12], [mint20], [mint21], [ubuntu20], [ubuntu21]"
	@echo "  [ubuntu22]"
	@echo "  [fedora37], [fedora38]"
	@echo "  [macos-x64_64], [macos-aarch64]"
	@echo "  [tumbleweed]"
	@echo "  [windows]"
	@echo "  [tarball]"
	@echo ""


clean :
	sudo rm -rf work

veryclean : clean
	rm -f $(tarball) $(tarball).sha256sum
	rm -f librewolf-*-*.en-US.*-x86_64.tar.bz2 librewolf-*-*.en-US.*-x86_64.tar.bz2.sha256sum librewolf-*-*.en-US.mac.*
	rm -f librewolf-*-*.en-US.*.x86_64.deb librewolf-*-*.en-US.*.x86_64.deb.sha256sum
	rm -f librewolf-*-*.*.x86_64.rpm librewolf-*-*.*.x86_64.rpm.sha256sum

prune :
	docker system prune --all --force

docker : docker-debian11 docker-debian12 docker-mint20 docker-mint21 docker-ubuntu20 docker-ubuntu21 docker-ubuntu22 docker-fedora39 docker-fedora40 docker-tumbleweed docker-macos-x86_64 docker-macos-aarch64 docker-dind

build :
	${MAKE} clean
	${MAKE} debian11
	${MAKE} clean
	${MAKE} debian12
	${MAKE} clean
	${MAKE} mint20
	${MAKE} clean
	${MAKE} mint21
	${MAKE} clean
	${MAKE} ubuntu20
	${MAKE} clean
	${MAKE} ubuntu21
	${MAKE} clean
	${MAKE} ubuntu22
	${MAKE} clean
	${MAKE} fedora39
	${MAKE} clean
	${MAKE} fedora40
	${MAKE} clean
	${MAKE} tumbleweed
	${MAKE} clean
	${MAKE} macos-x86_64
	${MAKE} clean
	${MAKE} macos-aarch64
	${MAKE} clean
	${MAKE} windows
	${MAKE} clean

push :
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/debian11
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/debian12
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/mint20
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/mint21
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/ubuntu20
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/ubuntu21
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/ubuntu22
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/fedora39
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/fedora40
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/tumbleweed
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/macos-x86_64
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/macos-aarch64
	docker push registry.gitlab.com/librewolf-community/browser/bsys5/dind

rmi :
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/debian11
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/debian12
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/mint20
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/mint21
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/ubuntu20
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/ubuntu21
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/ubuntu22
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/fedora39
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/fedora40
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/tumbleweed
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/macos-x86_64
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/macos-aarch64
	docker rmi registry.gitlab.com/librewolf-community/browser/bsys5/dind

update :
	@wget -q -O version "https://gitlab.com/librewolf-community/browser/source/-/raw/main/version"
	@wget -q -O source_release "https://gitlab.com/librewolf-community/browser/source/-/raw/main/release"
	@echo Source version: $(shell cat version)-$(shell cat source_release)
	@echo Bsys5 release: $(shell cat release)





## setting up the work folder
tarball=librewolf-$(version)-$(source_release).source.tar.gz
$(tarball) :
ifeq ($(SOURCE_URL),)
	wget -q -O $(tarball) "https://gitlab.com/api/v4/projects/32320088/packages/generic/librewolf-source/$(version)-$(source_release)/$(tarball)"
	wget -q -O $(tarball).sha256sum "https://gitlab.com/api/v4/projects/32320088/packages/generic/librewolf-source/$(version)-$(source_release)/$(tarball).sha256sum"
	sha256sum -c $(tarball).sha256sum
else
	wget -q -O $(tarball) "$(SOURCE_URL)"
endif
work : $(tarball)
	mkdir work
	(cd work && tar xf ../$(tarball))




#
# Linux
#

## debian11
docker-debian11 :
	${MAKE} -f assets/linux.mk distro=debian11 "distro_image=debian:bullseye" docker
debian11 :
	${MAKE} -f assets/linux.mk distro=debian11 build
	${MAKE} -f assets/linux.artifacts.mk distro=debian11 artifacts-deb
## debian12
docker-debian12 :
	${MAKE} -f assets/linux.mk distro=debian12 "distro_image=debian:bookworm" docker
debian12 :
	${MAKE} -f assets/linux.mk distro=debian12 build
	${MAKE} -f assets/linux.artifacts.mk distro=debian12 artifacts-deb
## mint20
docker-mint20 :
	${MAKE} -f assets/linux.mk distro=mint20 "distro_image=linuxmintd/mint20.2-amd64" docker
mint20 :
	${MAKE} -f assets/linux.mk distro=mint20 build
	${MAKE} -f assets/linux.artifacts.mk distro=mint20 artifacts-deb
## mint21
docker-mint21 :
	${MAKE} -f assets/linux.mk distro=mint21 "distro_image=linuxmintd/mint21-amd64" docker
mint21 :
	${MAKE} -f assets/linux.mk distro=mint21 build
	${MAKE} -f assets/linux.artifacts.mk distro=mint21 artifacts-deb
## ubuntu20
docker-ubuntu20 :
	${MAKE} -f assets/linux.mk distro=ubuntu20 "distro_image=ubuntu:focal" docker
ubuntu20 :
	${MAKE} -f assets/linux.mk distro=ubuntu20 build
	${MAKE} -f assets/linux.artifacts.mk distro=ubuntu20 artifacts-deb
## ubuntu21
docker-ubuntu21 :
	${MAKE} -f assets/linux.mk distro=ubuntu21 "distro_image=ubuntu:impish" docker
ubuntu21 :
	${MAKE} -f assets/linux.mk distro=ubuntu21 build
	${MAKE} -f assets/linux.artifacts.mk distro=ubuntu21 artifacts-deb
## ubuntu22
docker-ubuntu22 :
	${MAKE} -f assets/linux.mk distro=ubuntu22 "distro_image=ubuntu:jammy" docker
ubuntu22 :
	${MAKE} -f assets/linux.mk distro=ubuntu22 build
	${MAKE} -f assets/linux.artifacts.mk distro=ubuntu22 artifacts-deb
## fedora39
docker-fedora39 :
	${MAKE} -f assets/linux.mk distro=fedora39 "distro_image=fedora:39" docker
fedora39 :
	${MAKE} -f assets/linux.mk distro=fedora39 build
	${MAKE} -f assets/linux.artifacts.mk fc=fc39 distro=fedora39 artifacts-rpm
## fedora40
docker-fedora40 :
	${MAKE} -f assets/linux.mk distro=fedora40 "distro_image=fedora:40" docker
fedora40 :
	${MAKE} -f assets/linux.mk distro=fedora40 build
	${MAKE} -f assets/linux.artifacts.mk fc=fc40 distro=fedora40 artifacts-rpm
## opensuse tumbleweed
docker-tumbleweed :
	${MAKE} -f assets/linux.mk distro=tumbleweed "distro_image=opensuse/tumbleweed" docker
tumbleweed :
	${MAKE} -f assets/linux.mk distro=tumbleweed build
	${MAKE} -f assets/linux.artifacts.mk fc=tumbleweed distro=tumbleweed artifacts-rpm

## tarball
tarball :	
	${MAKE} -f assets/linux.mk distro=ubuntu20 build
	mv -v librewolf-$(full_version).en-US.ubuntu20-x86_64.tar.bz2 librewolf-$(full_version).en-US.generic-x86_64.tar.bz
	sha256sum librewolf-$(full_version).en-US.generic-x86_64.tar.bz > librewolf-$(full_version).en-US.generic-x86_64.tar.bz.sha256sum
	cat librewolf-$(full_version).en-US.generic-x86_64.tar.bz.sha256sum

#
# MacOS
#

## macos-x86_64
docker-macos-x86_64 :
	${MAKE} -f assets/macos.mk arch=x86_64 docker
macos-x86_64 :
	${MAKE} -f assets/macos.mk arch=x86_64 build

## macos-aarch64
docker-macos-aarch64 :
	${MAKE} -f assets/macos.mk arch=aarch64 docker
macos-aarch64 :
	${MAKE} -f assets/macos.mk arch=aarch64 build

## windows
docker-windows :
	${MAKE} -f assets/windows.mk docker
windows :
	${MAKE} -f assets/windows.mk build

#
# Docker in Docker (for GitLab CI)
#

docker-dind :
	docker build -f assets/dind.Dockerfile -t registry.gitlab.com/librewolf-community/browser/bsys5/dind:latest .
