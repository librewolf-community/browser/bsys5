# windows.mk - this one is quite a bit different from the others,
# but we can fix that later.

.PHONY : docker build

version:=$(shell cat version)
release:=$(shell cat release)
source_release:=$(shell cat source_release)
full_version:=$(version)-$(source_release)$(shell [ $(release) -gt 1 ] && echo "-$(release)")

outfile-exe=librewolf-$(full_version).en-US.win64-setup.exe
outfile-zip=librewolf-$(full_version).en-US.win64-portable.zip
outfiles=$(outfile-exe) $(outfile-zip)

distro=windows
image=registry.gitlab.com/librewolf-community/browser/bsys5/$(distro):latest

docker :

	docker build -t $(image) - < assets/windows.Dockerfile

build :

	pwd
	rm -rf windows
	git clone https://gitlab.com/librewolf-community/browser/windows.git

	( cd windows/linux && pwd && ${MAKE} fetch )

ifeq ($(use_docker),false)
	( cd windows/linux && ${MAKE} )
else
	docker run --rm -v $(shell pwd)/windows/linux:/work:rw registry.gitlab.com/librewolf-community/browser/bsys5/$(distro) sh -c "${MAKE} all"
endif

	cp -v windows/linux/$(outfiles) .

	sha256sum $(outfile-exe) > $(outfile-exe).sha256sum
	cat $(outfile-exe).sha256sum
	sha256sum $(outfile-zip) > $(outfile-zip).sha256sum
	cat $(outfile-zip).sha256sum

